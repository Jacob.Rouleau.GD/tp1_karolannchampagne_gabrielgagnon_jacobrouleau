﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyLaserBeam : MonoBehaviour
{
    public GameObject Laser;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Instantiate(Laser, transform.position, transform.rotation);
    }
}
