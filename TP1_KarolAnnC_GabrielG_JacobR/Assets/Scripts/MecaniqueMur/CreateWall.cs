﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateWall : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Wall;
    [SerializeField]
    private Transform m_SpawnWall;
    [SerializeField]
    private float m_CurrentTime = 5;
    private bool m_TimerCanStart = false;
    private bool m_CanPlace = true;

    public TMPro.TMP_Text TimeText;
    public GameObject m_CanvasText;

    void Update()
    {
        if (m_TimerCanStart)
        {
            TimeText.text = "Time: " + m_CurrentTime;

            m_CurrentTime -= Time.deltaTime;

            if (m_CurrentTime <= 0)
            {
                m_CanPlace = true;
                m_CanvasText.SetActive(false);
                m_CurrentTime = 5;
                
            }
        }

        if(Input.GetKeyDown(KeyCode.E) && m_CanPlace)
        {
            Instantiate(m_Wall, m_SpawnWall.transform.position, Quaternion.identity);//vef quaternion
            m_TimerCanStart = true;
            m_CanPlace = false;
            m_CanvasText.SetActive(true);
        }
    }
    //ignorer bullet?
    //perso passe travers, pas ennemis
}
