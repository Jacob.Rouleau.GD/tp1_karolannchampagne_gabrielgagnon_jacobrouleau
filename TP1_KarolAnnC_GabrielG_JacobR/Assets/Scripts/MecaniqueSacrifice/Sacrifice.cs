﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sacrifice : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Collider;
    private bool m_CanActive = true;
    public TMPro.TMP_Text YesNoText;

    private void Start()
    {
        YesNoText.text = "Can active?:" + "Yes";
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.E) && m_CanActive)
        {
            StartCoroutine("CoroutineSacrifice");
        }
    }

    IEnumerator CoroutineSacrifice()
    {
        m_CanActive = false;
        YesNoText.text = "Can active?:" + "No";
        m_Collider.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        m_Collider.SetActive(false);
        yield return new WaitForSeconds (5f);
        m_CanActive = true;
        YesNoText.text = "Can active?:" + "Yes";
    }
}
