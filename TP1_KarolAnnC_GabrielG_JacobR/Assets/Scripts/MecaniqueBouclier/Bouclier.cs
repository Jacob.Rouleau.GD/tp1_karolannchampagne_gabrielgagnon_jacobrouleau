﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouclier : MonoBehaviour
{
    [SerializeField]
    GameObject m_Bouclier;
    [SerializeField]
    int m_NbTouch = 0;

    public TMPro.TMP_Text TimeText;
    public GameObject m_CanvasText;

    // Update is called once per frame
    void Update()
    {
        TimeText.text = "Coup restant: " + m_NbTouch;
        if (m_NbTouch > 0)
        {
            m_CanvasText.SetActive(true);
        }
        else
        {
            m_CanvasText.SetActive(false);
        }

        if (Input.GetKey(KeyCode.E))
        {
            m_NbTouch = 3;
            gameObject.GetComponent<Collider>().enabled = false;
            m_Bouclier.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        m_NbTouch--;

        if (m_NbTouch <= 0)
        {
            m_Bouclier.SetActive(false);
            gameObject.GetComponent<Collider>().enabled = true;
        }
    }
}
