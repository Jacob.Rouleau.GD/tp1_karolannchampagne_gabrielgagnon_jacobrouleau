﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScale : MonoBehaviour
{
    [SerializeField]
    private bool m_TimerCanStart = false;
    [SerializeField]
    private float m_CurrentTime = 5;
    public TMPro.TMP_Text TimeText;
    public GameObject m_CanvasText;

    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            m_TimerCanStart = true;
            StartCoroutine("CoroutineSmaller");
        }

        if (m_TimerCanStart)
        {
            m_CanvasText.SetActive(true);
            TimeText.text = "Time: " + m_CurrentTime;
            m_CurrentTime -= Time.deltaTime;

            if (m_CurrentTime <= 0)
            {
                m_CurrentTime = m_CurrentTime / 60f;
                m_TimerCanStart = false;
                m_CurrentTime = 5f;
                m_CanvasText.SetActive(false);
            }
        }
 
    }

    IEnumerator CoroutineSmaller()
    {
        gameObject.transform.localScale *= 0.5f;

        yield return new WaitForSeconds(5f);

        gameObject.transform.localScale *= 2f;
    }
}
