﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSystem : MonoBehaviour
{
    private GameController gameController;
    public float m_Life;
    public TMPro.TMP_Text m_NumberLifeText;

    private void Update()
    {
        m_NumberLifeText.text = "Life: " + m_Life;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            m_Life -= 5f;
            Destroy(other.gameObject);

            if (m_Life <= 0)
            {
                m_NumberLifeText.text = "Life: " + "0";
                GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
                gameController = gameControllerObject.GetComponent<GameController>();
                gameController.GameOver();
                Destroy(gameObject);
            }
        }

        if (other.tag == "Life")
        {
            Debug.Log("Je vois le heal");
            m_Life += 3f;
            Destroy(other.gameObject);
        }
    }
}
