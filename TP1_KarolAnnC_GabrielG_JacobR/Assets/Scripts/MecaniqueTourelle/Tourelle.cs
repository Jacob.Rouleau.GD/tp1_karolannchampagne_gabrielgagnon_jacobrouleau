﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tourelle : MonoBehaviour
{
    public float speed;
    public float tilt;
    public Boundary boundary;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private bool CanShoot = false;

    private float nextFire;

    public float m_Timer = 0.1f;
    [SerializeField]
    private float m_CurrentTime;

    private void Start()
    {
        m_CurrentTime = m_Timer;
    }

    void Update()
    {
        m_CurrentTime -= Time.deltaTime;

        if (m_CurrentTime <= 0)
        {
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            m_CurrentTime = m_Timer;
        }

        //StartCoroutine("TestCoroutine");

        //if (Input.GetKey(KeyCode.N) && Time.time > nextFire)

        //{

        //  nextFire = Time.time + fireRate;
        // Instantiate(shot, shotSpawn.position, shotSpawn.rotation);


        // GetComponent<AudioSource>().Play();
        // }


        if (Input.GetKey(KeyCode.B))
        {
            transform.Rotate(Vector3.down * (Time.deltaTime * 50), Space.World);
        }

        if (Input.GetKey(KeyCode.M))
        {
            transform.Rotate(Vector3.up * (Time.deltaTime * 50), Space.World);
        }

        /* if (Input.GetKey(KeyCode.Alpha9))
         {
             Debug.Log(gameObject.transform.rotation.x);
             float variable =  gameObject.transform.rotation.x;
             variable ++;
             variable = gameObject.transform.position.x;
             Debug.Log(gameObject.transform.rotation.x);
         } */
    }

    /*IEnumerator TestCoroutine()
    {
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        yield return new WaitForSeconds(1);
        //nextFire = Time.time + fireRate;
        
       // yield return new WaitForSeconds(1);

    }*/
}
