﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderLaser : MonoBehaviour
{
    private float m_Descente;
    public TMPro.TMP_Text TimeText;
    public GameObject m_CanvasText;
    public string m_YesOrNo;

    void Update()
    {
        TimeText.text = "Activate " + m_YesOrNo;

        if (Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine("CoroutineUnderLaser");

            /*gameObject.GetComponent<Collider>().enabled = false;
            Vector3 pos = transform.position;
            pos.y = m_Descente;
            transform.position = pos;*/
        }
    }

    IEnumerator CoroutineUnderLaser()
    {
        m_YesOrNo = "Yes";
        gameObject.GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(1);
        gameObject.GetComponent<Collider>().enabled = true;
        m_YesOrNo = "No";
    }
}
