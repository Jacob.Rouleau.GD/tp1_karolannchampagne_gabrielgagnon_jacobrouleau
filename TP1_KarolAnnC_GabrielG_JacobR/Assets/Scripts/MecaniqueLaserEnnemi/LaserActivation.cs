﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserActivation : MonoBehaviour
{
    public GameObject LotLaser1;
    public GameObject LotLaser2;
    public GameObject LotLaser3;
    public GameObject LotLaser4;
    public float m_Timer = 0.1f;
    [SerializeField]
    private float m_CurrentTime;
    // Start is called before the first frame update
    void Start()
    {
        m_CurrentTime = m_Timer;
    }

    // Update is called once per frame
    void Update()
    {
         m_CurrentTime -= Time.deltaTime;

         if(m_CurrentTime <= -2)
         {
             LotLaser1.SetActive(true);
         }

         if(m_CurrentTime <= -12)
         {
             LotLaser1.SetActive(false);
         }

         if(m_CurrentTime <= -13)
         {
             LotLaser2.SetActive(true);
         }

         if(m_CurrentTime <= -20)
         {
             LotLaser2.SetActive(false);
         }

         if(m_CurrentTime <= -21)
         {
             LotLaser3.SetActive(true);
         }

         if(m_CurrentTime <= -27)
         {
             LotLaser4.SetActive(true);
         }

         if(m_CurrentTime <= -38)
         {
             LotLaser3.SetActive(false);
             LotLaser4.SetActive(false);
         }
    }
}
