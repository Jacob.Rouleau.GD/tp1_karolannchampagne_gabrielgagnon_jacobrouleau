﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invicible : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ColliderDestruction;
    [SerializeField]
    private bool m_TimerCanStart = false;
    [SerializeField]
    private float m_CurrentTime = 5;
    public TMPro.TMP_Text TimeText;
    public GameObject m_CanvasText;

    void Update()
    {
        if (Input.GetKey(KeyCode.E))
        {
            m_TimerCanStart = true;
            StartCoroutine("CoroutineInvincible");
        }

        if (m_TimerCanStart)
        {
            m_CanvasText.SetActive(true);
            TimeText.text = "Time: " + m_CurrentTime;
            m_CurrentTime -= Time.deltaTime;

            if (m_CurrentTime <= 0)
            {
                m_CurrentTime = m_CurrentTime / 60f;
                m_TimerCanStart = false;
                m_CurrentTime = 5f;
                m_CanvasText.SetActive(false);
            }
        }
    }

    IEnumerator CoroutineInvincible()
    {
        Collider collider = gameObject.GetComponent<Collider>();
        collider.enabled = false;
        m_ColliderDestruction.SetActive(true);
        yield return new WaitForSeconds(5f);
        m_ColliderDestruction.SetActive(false);
        collider.enabled = true;
    }
}
